/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/Calculator.ts":
/*!***************************!*\
  !*** ./src/Calculator.ts ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   Calculator: () => (/* binding */ Calculator),
/* harmony export */   Op: () => (/* binding */ Op)
/* harmony export */ });
/**
 * The binary operations supported by the calculator.
 */
var Op;
(function (Op) {
    /**
     * Addition.
     */
    Op[Op["Add"] = 0] = "Add";
    /**
     * Subtraction.
     */
    Op[Op["Sub"] = 1] = "Sub";
    /**
     * Multiplication.
     */
    Op[Op["Mul"] = 2] = "Mul";
    /**
     * Division.
     */
    Op[Op["Div"] = 3] = "Div";
})(Op || (Op = {}));
/**
 * A basic four-function calculator. UI logic is handled separately in
 * {@link CalculatorUI}.
 */
var Calculator = /** @class */ (function () {
    /**
     * In its initial state, the calculator's screen shows `0`, there is no
     * previous result or operation, and overwrite mode is enabled.
     */
    function Calculator() {
        this.lcd = '0';
        this.arg = null;
        this.lastOp = null;
        this.overwrite = true;
        this.repeat = false;
    }
    /**
     * Input a single digit.
     * @param x a single digit, 0-9
     */
    Calculator.prototype.digit = function (x) {
        if (this.overwrite) {
            this.lcd = x.toString();
            this.overwrite = false;
        }
        else {
            this.lcd += x;
        }
    };
    /**
     * Input a decimal point.
     */
    Calculator.prototype.decimal = function () {
        if (this.overwrite) {
            this.lcd = '0.';
            this.overwrite = false;
        }
        else if (this.lcd.indexOf('.') === -1) { // don't allow more than one '.'
            this.lcd += '.';
        }
    };
    Calculator.prototype.negate = function () {
        if (this.overwrite) {
            this.lcd = '0';
            this.overwrite = false;
        }
        else if (this.lcd !== '0') { // don't negate '0'
            if (this.lcd.charAt(0) === '-')
                this.lcd = this.lcd.substring(1);
            else
                this.lcd = '-' + this.lcd;
        }
    };
    /**
     * Input a binary operator. If there is a pending operation whose result has
     * not yet been displayed, update the screen to display that result. For
     * example, when a user inputs 2 + 4 + 8, the screen is updated to display 6
     * on the second + input.
     */
    Calculator.prototype.op = function (o) {
        this.overwrite = true;
        if (this.arg === null || this.repeat) { // if this is the first argument
            this.lastOp = o;
            this.arg = parseFloat(this.lcd);
        }
        else { // if this is the second argument
            switch (this.lastOp) {
                case Op.Add:
                    this.lcd = (this.arg + parseFloat(this.lcd)).toString();
                    break;
                case Op.Sub:
                    this.lcd = (this.arg - parseFloat(this.lcd)).toString();
                    break;
                case Op.Mul:
                    this.lcd = (this.arg * parseFloat(this.lcd)).toString();
                    break;
                case Op.Div:
                    this.lcd = (this.arg / parseFloat(this.lcd)).toString();
                    break;
            }
            this.lastOp = o;
            this.arg = parseFloat(this.lcd);
        }
        this.repeat = false;
    };
    /**
     * If the calculator is not in repeat mode, compute the result of the pending
     * expression if there is one. If the calculator is in repeat mode,
     * re-execute the previous operation.
     *
     * @see {@link repeat}
     */
    Calculator.prototype.equals = function () {
        // If `repeat` is disabled, this press of = will enable it. In that case,
        // the value currently on screen is the second argument, the one that's used
        // when repeating the operation.
        var oldLcd = parseFloat(this.lcd);
        // If `repeat` is disabled, then `this.arg` is the first argument to the
        // operation; if `repeat` is enabled, then it's the second argument.
        // This doesn't matter in the + and * cases because the result is the same
        // either way.
        switch (this.lastOp) {
            case Op.Add:
                this.lcd = (this.arg + parseFloat(this.lcd)).toString();
                break;
            case Op.Sub:
                if (this.repeat)
                    this.lcd = (parseFloat(this.lcd) - this.arg).toString();
                else
                    this.lcd = (this.arg - parseFloat(this.lcd)).toString();
                break;
            case Op.Mul:
                this.lcd = (this.arg * parseFloat(this.lcd)).toString();
                break;
            case Op.Div:
                if (this.repeat)
                    this.lcd = (parseFloat(this.lcd) / this.arg).toString();
                else
                    this.lcd = (this.arg / parseFloat(this.lcd)).toString();
                break;
        }
        // If `repeat` is disabled, we need to save the previous value of the screen
        // to use it as the second argument when repeating the operation.
        if (!this.repeat)
            this.arg = oldLcd;
        this.repeat = true;
        this.overwrite = true;
    };
    /**
     * Clear the screen, resetting it to 0. If in overwrite mode, reset the
     * entire calculator to its initial state.
     */
    Calculator.prototype.clear = function () {
        if (this.overwrite) {
            this.arg = null;
            this.lastOp = null;
            this.repeat = false;
        }
        this.lcd = '0';
        this.overwrite = true;
    };
    Calculator.prototype.cube = function () {
        if (this.overwrite) {
            this.lcd = (Math.pow(parseFloat(this.lcd), 3)).toString();
        }
        else {
            this.lcd = (Math.pow(parseFloat(this.lcd), 3)).toString();
            this.overwrite = true;
        }
    };
    Calculator.prototype.percent = function () {
        if (this.overwrite) {
            this.lcd = (parseFloat(this.lcd) / 100).toString();
        }
        else {
            this.lcd = (parseFloat(this.lcd) / 100).toString();
            this.overwrite = true;
        }
    };
    return Calculator;
}());



/***/ }),

/***/ "./src/CalculatorUI.ts":
/*!*****************************!*\
  !*** ./src/CalculatorUI.ts ***!
  \*****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   CalculatorUI: () => (/* binding */ CalculatorUI)
/* harmony export */ });
/* harmony import */ var _Calculator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Calculator */ "./src/Calculator.ts");
var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

/**
 * The UI logic for the calculator interface, which just updates the HTML
 * element representing the display every time the calculator's state changes.
 * Button actions are bound in {@link Main}.
 */
var CalculatorUI = /** @class */ (function (_super) {
    __extends(CalculatorUI, _super);
    function CalculatorUI(id) {
        var _this = _super.call(this) || this;
        _this.lcdDisplay = document.getElementById(id);
        _this.lcdDisplay.innerHTML = _this.lcd;
        return _this;
    }
    CalculatorUI.prototype.digit = function (x) {
        _super.prototype.digit.call(this, x);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    CalculatorUI.prototype.decimal = function () {
        _super.prototype.decimal.call(this);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    CalculatorUI.prototype.negate = function () {
        _super.prototype.negate.call(this);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    CalculatorUI.prototype.op = function (o) {
        _super.prototype.op.call(this, o);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    CalculatorUI.prototype.equals = function () {
        _super.prototype.equals.call(this);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    CalculatorUI.prototype.clear = function () {
        _super.prototype.clear.call(this);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    CalculatorUI.prototype.cube = function () {
        _super.prototype.cube.call(this);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    CalculatorUI.prototype.percent = function () {
        _super.prototype.percent.call(this);
        this.lcdDisplay.innerHTML = this.lcd.toString();
    };
    return CalculatorUI;
}(_Calculator__WEBPACK_IMPORTED_MODULE_0__.Calculator));



/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*********************!*\
  !*** ./src/Main.ts ***!
  \*********************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CalculatorUI__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CalculatorUI */ "./src/CalculatorUI.ts");
/* harmony import */ var _Calculator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Calculator */ "./src/Calculator.ts");


window.onload = function () {
    var calcUI = new _CalculatorUI__WEBPACK_IMPORTED_MODULE_0__.CalculatorUI('lcd');
    document.getElementById('1').onclick = function () { return calcUI.digit(1); };
    document.getElementById('2').onclick = function () { return calcUI.digit(2); };
    document.getElementById('3').onclick = function () { return calcUI.digit(3); };
    document.getElementById('4').onclick = function () { return calcUI.digit(4); };
    document.getElementById('5').onclick = function () { return calcUI.digit(5); };
    document.getElementById('6').onclick = function () { return calcUI.digit(6); };
    document.getElementById('7').onclick = function () { return calcUI.digit(7); };
    document.getElementById('8').onclick = function () { return calcUI.digit(8); };
    document.getElementById('9').onclick = function () { return calcUI.digit(9); };
    document.getElementById('0').onclick = function () { return calcUI.digit(0); };
    document.getElementById('+-').onclick = function () { return calcUI.negate(); };
    document.getElementById('.').onclick = function () { return calcUI.decimal(); };
    document.getElementById('+').onclick = function () { return calcUI.op(_Calculator__WEBPACK_IMPORTED_MODULE_1__.Op.Add); };
    document.getElementById('-').onclick = function () { return calcUI.op(_Calculator__WEBPACK_IMPORTED_MODULE_1__.Op.Sub); };
    document.getElementById('*').onclick = function () { return calcUI.op(_Calculator__WEBPACK_IMPORTED_MODULE_1__.Op.Mul); };
    document.getElementById('/').onclick = function () { return calcUI.op(_Calculator__WEBPACK_IMPORTED_MODULE_1__.Op.Div); };
    document.getElementById('=').onclick = function () { return calcUI.equals(); };
    document.getElementById('C').onclick = function () { return calcUI.clear(); };
    document.getElementById('^3').onclick = function () { return calcUI.cube(); };
    document.getElementById('%').onclick = function () { return calcUI.percent(); };
};

})();

/******/ })()
;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnVuZGxlLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUFBOztHQUVHO0FBQ0gsSUFBWSxFQW9CWDtBQXBCRCxXQUFZLEVBQUU7SUFDWjs7T0FFRztJQUNILHlCQUFHO0lBRUg7O09BRUc7SUFDSCx5QkFBRztJQUVIOztPQUVHO0lBQ0gseUJBQUc7SUFFSDs7T0FFRztJQUNILHlCQUFHO0FBQ0wsQ0FBQyxFQXBCVyxFQUFFLEtBQUYsRUFBRSxRQW9CYjtBQUVEOzs7R0FHRztBQUNIO0lBa0NFOzs7T0FHRztJQUNIO1FBQ0UsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQztRQUNoQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztRQUNuQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQ7OztPQUdHO0lBQ0gsMEJBQUssR0FBTCxVQUFNLENBQVM7UUFDYixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7U0FDeEI7YUFBTTtZQUNMLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO1NBQ2Y7SUFDSCxDQUFDO0lBRUQ7O09BRUc7SUFDSCw0QkFBTyxHQUFQO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1NBQ3hCO2FBQU0sSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxFQUFFLGdDQUFnQztZQUN6RSxJQUFJLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQztTQUNqQjtJQUNILENBQUM7SUFFRCwyQkFBTSxHQUFOO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBQ2YsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7U0FDeEI7YUFBTSxJQUFJLElBQUksQ0FBQyxHQUFHLEtBQUssR0FBRyxFQUFFLEVBQUUsbUJBQW1CO1lBQ2hELElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssR0FBRztnQkFDNUIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQzs7Z0JBRWpDLElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7U0FDN0I7SUFDSCxDQUFDO0lBRUQ7Ozs7O09BS0c7SUFDSCx1QkFBRSxHQUFGLFVBQUcsQ0FBSztRQUNOLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksSUFBSSxDQUFDLEdBQUcsS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFFLGdDQUFnQztZQUN0RSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDakM7YUFBTSxFQUFFLGlDQUFpQztZQUN4QyxRQUFRLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ25CLEtBQUssRUFBRSxDQUFDLEdBQUc7b0JBQUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUFDLE1BQU07Z0JBQzVFLEtBQUssRUFBRSxDQUFDLEdBQUc7b0JBQUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUFDLE1BQU07Z0JBQzVFLEtBQUssRUFBRSxDQUFDLEdBQUc7b0JBQUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUFDLE1BQU07Z0JBQzVFLEtBQUssRUFBRSxDQUFDLEdBQUc7b0JBQUUsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEdBQUcsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUFDLE1BQU07YUFDN0U7WUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUNoQixJQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDakM7UUFDRCxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQ7Ozs7OztPQU1HO0lBQ0gsMkJBQU0sR0FBTjtRQUNFLHlFQUF5RTtRQUN6RSw0RUFBNEU7UUFDNUUsZ0NBQWdDO1FBQ2hDLElBQU0sTUFBTSxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7UUFFcEMsd0VBQXdFO1FBQ3hFLG9FQUFvRTtRQUNwRSwwRUFBMEU7UUFDMUUsY0FBYztRQUNkLFFBQVEsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNuQixLQUFLLEVBQUUsQ0FBQyxHQUFHO2dCQUFFLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFBQyxNQUFNO1lBQzVFLEtBQUssRUFBRSxDQUFDLEdBQUc7Z0JBQ1QsSUFBSSxJQUFJLENBQUMsTUFBTTtvQkFDYixJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7O29CQUV4RCxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQzFELE1BQU07WUFDUixLQUFLLEVBQUUsQ0FBQyxHQUFHO2dCQUFFLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFBQyxNQUFNO1lBQzVFLEtBQUssRUFBRSxDQUFDLEdBQUc7Z0JBQ1QsSUFBSSxJQUFJLENBQUMsTUFBTTtvQkFDYixJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7O29CQUV4RCxJQUFJLENBQUMsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQzFELE1BQU07U0FDVDtRQUVELDRFQUE0RTtRQUM1RSxpRUFBaUU7UUFDakUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNO1lBQ2QsSUFBSSxDQUFDLEdBQUcsR0FBRyxNQUFNLENBQUM7UUFFcEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7UUFDbkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7SUFDeEIsQ0FBQztJQUVEOzs7T0FHRztJQUNILDBCQUFLLEdBQUw7UUFDRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUM7WUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7WUFDbkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7U0FDckI7UUFDRCxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO0lBQ3hCLENBQUM7SUFFRCx5QkFBSSxHQUFKO1FBQ0UsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2xCLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxtQkFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBSSxDQUFDLEVBQUMsQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNuRDthQUFNO1lBQ0wsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLG1CQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFJLENBQUMsRUFBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ2xELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQztJQUVELDRCQUFPLEdBQVA7UUFDRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDbEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDcEQ7YUFBTTtZQUNMLElBQUksQ0FBQyxHQUFHLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ25ELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO0lBQ0gsQ0FBQztJQUNILGlCQUFDO0FBQUQsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNsTjZDO0FBRTlDOzs7O0dBSUc7QUFDSDtJQUFrQyxnQ0FBVTtJQU0xQyxzQkFBWSxFQUFVO1FBQXRCLFlBQ0UsaUJBQU8sU0FHUjtRQUZDLEtBQUksQ0FBQyxVQUFVLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM5QyxLQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxLQUFJLENBQUMsR0FBRyxDQUFDOztJQUN2QyxDQUFDO0lBRUQsNEJBQUssR0FBTCxVQUFNLENBQVM7UUFDYixpQkFBTSxLQUFLLFlBQUMsQ0FBQyxDQUFDLENBQUM7UUFDZixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFRCw4QkFBTyxHQUFQO1FBQ0UsaUJBQU0sT0FBTyxXQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsRCxDQUFDO0lBRUQsNkJBQU0sR0FBTjtRQUNFLGlCQUFNLE1BQU0sV0FBRSxDQUFDO1FBQ2YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsRCxDQUFDO0lBRUQseUJBQUUsR0FBRixVQUFHLENBQUs7UUFDTixpQkFBTSxFQUFFLFlBQUMsQ0FBQyxDQUFDLENBQUM7UUFDWixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFRCw2QkFBTSxHQUFOO1FBQ0UsaUJBQU0sTUFBTSxXQUFFLENBQUM7UUFDZixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFRCw0QkFBSyxHQUFMO1FBQ0UsaUJBQU0sS0FBSyxXQUFFLENBQUM7UUFDZCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFRCwyQkFBSSxHQUFKO1FBQ0UsaUJBQU0sSUFBSSxXQUFFLENBQUM7UUFDYixJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ2xELENBQUM7SUFFRCw4QkFBTyxHQUFQO1FBQ0UsaUJBQU0sT0FBTyxXQUFFLENBQUM7UUFDaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNsRCxDQUFDO0lBQ0gsbUJBQUM7QUFBRCxDQUFDLENBbkRpQyxtREFBVSxHQW1EM0M7Ozs7Ozs7O1VDMUREO1VBQ0E7O1VBRUE7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7VUFDQTtVQUNBO1VBQ0E7O1VBRUE7VUFDQTs7VUFFQTtVQUNBO1VBQ0E7Ozs7O1dDdEJBO1dBQ0E7V0FDQTtXQUNBO1dBQ0EseUNBQXlDLHdDQUF3QztXQUNqRjtXQUNBO1dBQ0E7Ozs7O1dDUEE7Ozs7O1dDQUE7V0FDQTtXQUNBO1dBQ0EsdURBQXVELGlCQUFpQjtXQUN4RTtXQUNBLGdEQUFnRCxhQUFhO1dBQzdEOzs7Ozs7Ozs7Ozs7O0FDTjhDO0FBQ1o7QUFFbEMsTUFBTSxDQUFDLE1BQU0sR0FBRztJQUNkLElBQU0sTUFBTSxHQUFHLElBQUksdURBQVksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQWYsQ0FBZSxDQUFDO0lBQzdELFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxHQUFHLGNBQU0sYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBZixDQUFlLENBQUM7SUFDN0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsY0FBTSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFmLENBQWUsQ0FBQztJQUM3RCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQWYsQ0FBZSxDQUFDO0lBQzdELFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxHQUFHLGNBQU0sYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBZixDQUFlLENBQUM7SUFDN0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsY0FBTSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFmLENBQWUsQ0FBQztJQUM3RCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQWYsQ0FBZSxDQUFDO0lBQzdELFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxHQUFHLGNBQU0sYUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBZixDQUFlLENBQUM7SUFDN0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsY0FBTSxhQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFmLENBQWUsQ0FBQztJQUM3RCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQWYsQ0FBZSxDQUFDO0lBQzdELFFBQVEsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxHQUFHLGNBQU0sYUFBTSxDQUFDLE1BQU0sRUFBRSxFQUFmLENBQWUsQ0FBQztJQUM5RCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxPQUFPLEVBQUUsRUFBaEIsQ0FBZ0IsQ0FBQztJQUM5RCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxFQUFFLENBQUMsMkNBQUUsQ0FBQyxHQUFHLENBQUMsRUFBakIsQ0FBaUIsQ0FBQztJQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxFQUFFLENBQUMsMkNBQUUsQ0FBQyxHQUFHLENBQUMsRUFBakIsQ0FBaUIsQ0FBQztJQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxFQUFFLENBQUMsMkNBQUUsQ0FBQyxHQUFHLENBQUMsRUFBakIsQ0FBaUIsQ0FBQztJQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxFQUFFLENBQUMsMkNBQUUsQ0FBQyxHQUFHLENBQUMsRUFBakIsQ0FBaUIsQ0FBQztJQUMvRCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxNQUFNLEVBQUUsRUFBZixDQUFlLENBQUM7SUFDN0QsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLEdBQUcsY0FBTSxhQUFNLENBQUMsS0FBSyxFQUFFLEVBQWQsQ0FBYyxDQUFDO0lBQzVELFFBQVEsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxHQUFHLGNBQU0sYUFBTSxDQUFDLElBQUksRUFBRSxFQUFiLENBQWEsQ0FBQztJQUM1RCxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sR0FBRyxjQUFNLGFBQU0sQ0FBQyxPQUFPLEVBQUUsRUFBaEIsQ0FBZ0IsQ0FBQztBQUNoRSxDQUFDLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvQ2FsY3VsYXRvci50cyIsIndlYnBhY2s6Ly8vLi9zcmMvQ2FsY3VsYXRvclVJLnRzIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCIsIndlYnBhY2s6Ly8vd2VicGFjay9ydW50aW1lL2RlZmluZSBwcm9wZXJ0eSBnZXR0ZXJzIiwid2VicGFjazovLy93ZWJwYWNrL3J1bnRpbWUvaGFzT3duUHJvcGVydHkgc2hvcnRoYW5kIiwid2VicGFjazovLy93ZWJwYWNrL3J1bnRpbWUvbWFrZSBuYW1lc3BhY2Ugb2JqZWN0Iiwid2VicGFjazovLy8uL3NyYy9NYWluLnRzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogVGhlIGJpbmFyeSBvcGVyYXRpb25zIHN1cHBvcnRlZCBieSB0aGUgY2FsY3VsYXRvci5cbiAqL1xuZXhwb3J0IGVudW0gT3Age1xuICAvKipcbiAgICogQWRkaXRpb24uXG4gICAqL1xuICBBZGQsXG5cbiAgLyoqXG4gICAqIFN1YnRyYWN0aW9uLlxuICAgKi9cbiAgU3ViLFxuXG4gIC8qKlxuICAgKiBNdWx0aXBsaWNhdGlvbi5cbiAgICovXG4gIE11bCxcblxuICAvKipcbiAgICogRGl2aXNpb24uXG4gICAqL1xuICBEaXZcbn1cblxuLyoqXG4gKiBBIGJhc2ljIGZvdXItZnVuY3Rpb24gY2FsY3VsYXRvci4gVUkgbG9naWMgaXMgaGFuZGxlZCBzZXBhcmF0ZWx5IGluXG4gKiB7QGxpbmsgQ2FsY3VsYXRvclVJfS5cbiAqL1xuZXhwb3J0IGNsYXNzIENhbGN1bGF0b3Ige1xuICAvKipcbiAgICogVGhlIGNvbnRlbnRzIG9mIHRoZSBjYWxjdWxhdG9yJ3MgTENEIGRpc3BsYXkuXG4gICAqL1xuICBsY2Q6IHN0cmluZztcblxuICAvKipcbiAgICogVGhlIHJlc3VsdCBvZiB0aGUgbGFzdCBvcGVyYXRpb24gaWYgYHJlcGVhdGAgaXMgYGZhbHNlYCwgb3IgdGhlIHNlY29uZFxuICAgKiBhcmd1bWVudCBvZiB0aGUgbGFzdCBvcGVyYXRpb24gaWYgYHJlcGVhdGAgaXMgYHRydWVgLlxuICAgKi9cbiAgYXJnOiBudW1iZXI7XG5cbiAgLyoqXG4gICAqIFRoZSBsYXN0IG9wZXJhdGlvbiB0aGF0IHRoZSB1c2VyIGVudGVyZWQuXG4gICAqL1xuICBsYXN0T3A6IE9wO1xuXG4gIC8qKlxuICAgKiBJZiBgdHJ1ZWAsIHRoZSBjYWxjdWxhdG9yIGlzIGluIFwib3ZlcndyaXRlIG1vZGVcIjsgaWYgYGZhbHNlYCwgdGhlXG4gICAqIGNhbGN1bGF0b3IgaXMgaW4gXCJhcHBlbmQgbW9kZVwiLiBJbiBvdmVyd3JpdGUgbW9kZSwgdGhlIG5leHQgaW5wdXQgcmVwbGFjZXNcbiAgICogdGhlIGN1cnJlbnQgc2NyZWVuIGNvbnRlbnRzOyBpbiBhcHBlbmQgbW9kZSwgdGhlIG5leHQgaW5wdXQgYXBwZW5kcyB0byB0aGVcbiAgICogY3VycmVudCBzY3JlZW4gY29udGVudHMuXG4gICAqL1xuICBvdmVyd3JpdGU6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIElmIGB0cnVlYCwgdGhlIGNhbGN1bGF0b3IgaXMgaW4gXCJyZXBlYXQgbW9kZVwiLiBJbiByZXBlYXQgbW9kZSwgd2hlbiB0aGUgPVxuICAgKiBidXR0b24gaXMgcHJlc3NlZCwgdGhlIHNjcmVlbiBpcyB1cGRhdGVkIGJ5IHJlLWV4ZWN1dGluZyB0aGUgcHJldmlvdXNcbiAgICogb3BlcmF0aW9uIHdpdGggdGhlIHNhbWUgcmlnaHQtaGFuZCBhcmd1bWVudCBhcyBsYXN0IHRpbWUuIEZvciBleGFtcGxlLCBpZlxuICAgKiB0aGUgcHJldmlvdXMgb3BlcmF0aW9uIHdhcyAzICsgNSBhbmQgdGhlIGNhbGN1bGF0b3IgaXMgaW4gcmVwZWF0IG1vZGUsXG4gICAqIHByZXNzaW5nID0gd2lsbCB1cGRhdGUgdGhlIHNjcmVlbiB3aXRoIHRoZSBudW1iZXIgMTMuXG4gICAqL1xuICByZXBlYXQ6IGJvb2xlYW47XG5cbiAgLyoqXG4gICAqIEluIGl0cyBpbml0aWFsIHN0YXRlLCB0aGUgY2FsY3VsYXRvcidzIHNjcmVlbiBzaG93cyBgMGAsIHRoZXJlIGlzIG5vXG4gICAqIHByZXZpb3VzIHJlc3VsdCBvciBvcGVyYXRpb24sIGFuZCBvdmVyd3JpdGUgbW9kZSBpcyBlbmFibGVkLlxuICAgKi9cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgdGhpcy5sY2QgPSAnMCc7XG4gICAgdGhpcy5hcmcgPSBudWxsO1xuICAgIHRoaXMubGFzdE9wID0gbnVsbDtcbiAgICB0aGlzLm92ZXJ3cml0ZSA9IHRydWU7XG4gICAgdGhpcy5yZXBlYXQgPSBmYWxzZTtcbiAgfVxuXG4gIC8qKlxuICAgKiBJbnB1dCBhIHNpbmdsZSBkaWdpdC5cbiAgICogQHBhcmFtIHggYSBzaW5nbGUgZGlnaXQsIDAtOVxuICAgKi9cbiAgZGlnaXQoeDogbnVtYmVyKTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3ZlcndyaXRlKSB7XG4gICAgICB0aGlzLmxjZCA9IHgudG9TdHJpbmcoKTtcbiAgICAgIHRoaXMub3ZlcndyaXRlID0gZmFsc2U7XG4gICAgfSBlbHNlIHtcbiAgICAgIHRoaXMubGNkICs9IHg7XG4gICAgfVxuICB9XG5cbiAgLyoqXG4gICAqIElucHV0IGEgZGVjaW1hbCBwb2ludC5cbiAgICovXG4gIGRlY2ltYWwoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3ZlcndyaXRlKSB7XG4gICAgICB0aGlzLmxjZCA9ICcwLic7XG4gICAgICB0aGlzLm92ZXJ3cml0ZSA9IGZhbHNlO1xuICAgIH0gZWxzZSBpZiAodGhpcy5sY2QuaW5kZXhPZignLicpID09PSAtMSkgeyAvLyBkb24ndCBhbGxvdyBtb3JlIHRoYW4gb25lICcuJ1xuICAgICAgdGhpcy5sY2QgKz0gJy4nO1xuICAgIH1cbiAgfVxuXG4gIG5lZ2F0ZSgpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vdmVyd3JpdGUpIHtcbiAgICAgIHRoaXMubGNkID0gJzAnO1xuICAgICAgdGhpcy5vdmVyd3JpdGUgPSBmYWxzZTtcbiAgICB9IGVsc2UgaWYgKHRoaXMubGNkICE9PSAnMCcpIHsgLy8gZG9uJ3QgbmVnYXRlICcwJ1xuICAgICAgaWYgKHRoaXMubGNkLmNoYXJBdCgwKSA9PT0gJy0nKVxuICAgICAgICB0aGlzLmxjZCA9IHRoaXMubGNkLnN1YnN0cmluZygxKTtcbiAgICAgIGVsc2VcbiAgICAgICAgdGhpcy5sY2QgPSAnLScgKyB0aGlzLmxjZDtcbiAgICB9XG4gIH1cblxuICAvKipcbiAgICogSW5wdXQgYSBiaW5hcnkgb3BlcmF0b3IuIElmIHRoZXJlIGlzIGEgcGVuZGluZyBvcGVyYXRpb24gd2hvc2UgcmVzdWx0IGhhc1xuICAgKiBub3QgeWV0IGJlZW4gZGlzcGxheWVkLCB1cGRhdGUgdGhlIHNjcmVlbiB0byBkaXNwbGF5IHRoYXQgcmVzdWx0LiBGb3JcbiAgICogZXhhbXBsZSwgd2hlbiBhIHVzZXIgaW5wdXRzIDIgKyA0ICsgOCwgdGhlIHNjcmVlbiBpcyB1cGRhdGVkIHRvIGRpc3BsYXkgNlxuICAgKiBvbiB0aGUgc2Vjb25kICsgaW5wdXQuXG4gICAqL1xuICBvcChvOiBPcCk6IHZvaWQge1xuICAgIHRoaXMub3ZlcndyaXRlID0gdHJ1ZTtcbiAgICBpZiAodGhpcy5hcmcgPT09IG51bGwgfHwgdGhpcy5yZXBlYXQpIHsgLy8gaWYgdGhpcyBpcyB0aGUgZmlyc3QgYXJndW1lbnRcbiAgICAgIHRoaXMubGFzdE9wID0gbztcbiAgICAgIHRoaXMuYXJnID0gcGFyc2VGbG9hdCh0aGlzLmxjZCk7XG4gICAgfSBlbHNlIHsgLy8gaWYgdGhpcyBpcyB0aGUgc2Vjb25kIGFyZ3VtZW50XG4gICAgICBzd2l0Y2ggKHRoaXMubGFzdE9wKSB7XG4gICAgICAgIGNhc2UgT3AuQWRkOiB0aGlzLmxjZCA9ICh0aGlzLmFyZyArIHBhcnNlRmxvYXQodGhpcy5sY2QpKS50b1N0cmluZygpOyBicmVhaztcbiAgICAgICAgY2FzZSBPcC5TdWI6IHRoaXMubGNkID0gKHRoaXMuYXJnIC0gcGFyc2VGbG9hdCh0aGlzLmxjZCkpLnRvU3RyaW5nKCk7IGJyZWFrO1xuICAgICAgICBjYXNlIE9wLk11bDogdGhpcy5sY2QgPSAodGhpcy5hcmcgKiBwYXJzZUZsb2F0KHRoaXMubGNkKSkudG9TdHJpbmcoKTsgYnJlYWs7XG4gICAgICAgIGNhc2UgT3AuRGl2OiB0aGlzLmxjZCA9ICh0aGlzLmFyZyAvIHBhcnNlRmxvYXQodGhpcy5sY2QpKS50b1N0cmluZygpOyBicmVhaztcbiAgICAgIH1cbiAgICAgIHRoaXMubGFzdE9wID0gbztcbiAgICAgIHRoaXMuYXJnID0gcGFyc2VGbG9hdCh0aGlzLmxjZCk7XG4gICAgfVxuICAgIHRoaXMucmVwZWF0ID0gZmFsc2U7XG4gIH1cblxuICAvKipcbiAgICogSWYgdGhlIGNhbGN1bGF0b3IgaXMgbm90IGluIHJlcGVhdCBtb2RlLCBjb21wdXRlIHRoZSByZXN1bHQgb2YgdGhlIHBlbmRpbmdcbiAgICogZXhwcmVzc2lvbiBpZiB0aGVyZSBpcyBvbmUuIElmIHRoZSBjYWxjdWxhdG9yIGlzIGluIHJlcGVhdCBtb2RlLFxuICAgKiByZS1leGVjdXRlIHRoZSBwcmV2aW91cyBvcGVyYXRpb24uXG4gICAqXG4gICAqIEBzZWUge0BsaW5rIHJlcGVhdH1cbiAgICovXG4gIGVxdWFscygpOiB2b2lkIHtcbiAgICAvLyBJZiBgcmVwZWF0YCBpcyBkaXNhYmxlZCwgdGhpcyBwcmVzcyBvZiA9IHdpbGwgZW5hYmxlIGl0LiBJbiB0aGF0IGNhc2UsXG4gICAgLy8gdGhlIHZhbHVlIGN1cnJlbnRseSBvbiBzY3JlZW4gaXMgdGhlIHNlY29uZCBhcmd1bWVudCwgdGhlIG9uZSB0aGF0J3MgdXNlZFxuICAgIC8vIHdoZW4gcmVwZWF0aW5nIHRoZSBvcGVyYXRpb24uXG4gICAgY29uc3Qgb2xkTGNkID0gcGFyc2VGbG9hdCh0aGlzLmxjZCk7XG5cbiAgICAvLyBJZiBgcmVwZWF0YCBpcyBkaXNhYmxlZCwgdGhlbiBgdGhpcy5hcmdgIGlzIHRoZSBmaXJzdCBhcmd1bWVudCB0byB0aGVcbiAgICAvLyBvcGVyYXRpb247IGlmIGByZXBlYXRgIGlzIGVuYWJsZWQsIHRoZW4gaXQncyB0aGUgc2Vjb25kIGFyZ3VtZW50LlxuICAgIC8vIFRoaXMgZG9lc24ndCBtYXR0ZXIgaW4gdGhlICsgYW5kICogY2FzZXMgYmVjYXVzZSB0aGUgcmVzdWx0IGlzIHRoZSBzYW1lXG4gICAgLy8gZWl0aGVyIHdheS5cbiAgICBzd2l0Y2ggKHRoaXMubGFzdE9wKSB7XG4gICAgICBjYXNlIE9wLkFkZDogdGhpcy5sY2QgPSAodGhpcy5hcmcgKyBwYXJzZUZsb2F0KHRoaXMubGNkKSkudG9TdHJpbmcoKTsgYnJlYWs7XG4gICAgICBjYXNlIE9wLlN1YjpcbiAgICAgICAgaWYgKHRoaXMucmVwZWF0KVxuICAgICAgICAgIHRoaXMubGNkID0gKHBhcnNlRmxvYXQodGhpcy5sY2QpIC0gdGhpcy5hcmcpLnRvU3RyaW5nKCk7XG4gICAgICAgIGVsc2VcbiAgICAgICAgICB0aGlzLmxjZCA9ICh0aGlzLmFyZyAtIHBhcnNlRmxvYXQodGhpcy5sY2QpKS50b1N0cmluZygpO1xuICAgICAgICBicmVhaztcbiAgICAgIGNhc2UgT3AuTXVsOiB0aGlzLmxjZCA9ICh0aGlzLmFyZyAqIHBhcnNlRmxvYXQodGhpcy5sY2QpKS50b1N0cmluZygpOyBicmVhaztcbiAgICAgIGNhc2UgT3AuRGl2OlxuICAgICAgICBpZiAodGhpcy5yZXBlYXQpXG4gICAgICAgICAgdGhpcy5sY2QgPSAocGFyc2VGbG9hdCh0aGlzLmxjZCkgLyB0aGlzLmFyZykudG9TdHJpbmcoKTtcbiAgICAgICAgZWxzZVxuICAgICAgICAgIHRoaXMubGNkID0gKHRoaXMuYXJnIC8gcGFyc2VGbG9hdCh0aGlzLmxjZCkpLnRvU3RyaW5nKCk7XG4gICAgICAgIGJyZWFrO1xuICAgIH1cblxuICAgIC8vIElmIGByZXBlYXRgIGlzIGRpc2FibGVkLCB3ZSBuZWVkIHRvIHNhdmUgdGhlIHByZXZpb3VzIHZhbHVlIG9mIHRoZSBzY3JlZW5cbiAgICAvLyB0byB1c2UgaXQgYXMgdGhlIHNlY29uZCBhcmd1bWVudCB3aGVuIHJlcGVhdGluZyB0aGUgb3BlcmF0aW9uLlxuICAgIGlmICghdGhpcy5yZXBlYXQpXG4gICAgICB0aGlzLmFyZyA9IG9sZExjZDtcblxuICAgIHRoaXMucmVwZWF0ID0gdHJ1ZTtcbiAgICB0aGlzLm92ZXJ3cml0ZSA9IHRydWU7XG4gIH1cblxuICAvKipcbiAgICogQ2xlYXIgdGhlIHNjcmVlbiwgcmVzZXR0aW5nIGl0IHRvIDAuIElmIGluIG92ZXJ3cml0ZSBtb2RlLCByZXNldCB0aGVcbiAgICogZW50aXJlIGNhbGN1bGF0b3IgdG8gaXRzIGluaXRpYWwgc3RhdGUuXG4gICAqL1xuICBjbGVhcigpOiB2b2lkIHtcbiAgICBpZiAodGhpcy5vdmVyd3JpdGUpIHtcbiAgICAgIHRoaXMuYXJnID0gbnVsbDtcbiAgICAgIHRoaXMubGFzdE9wID0gbnVsbDtcbiAgICAgIHRoaXMucmVwZWF0ID0gZmFsc2U7XG4gICAgfVxuICAgIHRoaXMubGNkID0gJzAnO1xuICAgIHRoaXMub3ZlcndyaXRlID0gdHJ1ZTtcbiAgfVxuXG4gIGN1YmUoKTogdm9pZCB7XG4gICAgaWYgKHRoaXMub3ZlcndyaXRlKSB7XG4gICAgICB0aGlzLmxjZCA9IChwYXJzZUZsb2F0KHRoaXMubGNkKSAqKiAzKS50b1N0cmluZygpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmxjZCA9IChwYXJzZUZsb2F0KHRoaXMubGNkKSAqKiAzKS50b1N0cmluZygpO1xuICAgICAgdGhpcy5vdmVyd3JpdGUgPSB0cnVlO1xuICAgIH1cbiAgfVxuXG4gIHBlcmNlbnQoKTp2b2lkIHtcbiAgICBpZiAodGhpcy5vdmVyd3JpdGUpIHtcbiAgICAgIHRoaXMubGNkID0gKHBhcnNlRmxvYXQodGhpcy5sY2QpIC8gMTAwKS50b1N0cmluZygpO1xuICAgIH0gZWxzZSB7XG4gICAgICB0aGlzLmxjZCA9IChwYXJzZUZsb2F0KHRoaXMubGNkKSAvIDEwMCkudG9TdHJpbmcoKTtcbiAgICAgIHRoaXMub3ZlcndyaXRlID0gdHJ1ZTtcbiAgICB9XG4gIH1cbn0iLCJpbXBvcnQgeyBDYWxjdWxhdG9yLCBPcCB9IGZyb20gJy4vQ2FsY3VsYXRvcic7XG5cbi8qKlxuICogVGhlIFVJIGxvZ2ljIGZvciB0aGUgY2FsY3VsYXRvciBpbnRlcmZhY2UsIHdoaWNoIGp1c3QgdXBkYXRlcyB0aGUgSFRNTFxuICogZWxlbWVudCByZXByZXNlbnRpbmcgdGhlIGRpc3BsYXkgZXZlcnkgdGltZSB0aGUgY2FsY3VsYXRvcidzIHN0YXRlIGNoYW5nZXMuXG4gKiBCdXR0b24gYWN0aW9ucyBhcmUgYm91bmQgaW4ge0BsaW5rIE1haW59LlxuICovXG5leHBvcnQgY2xhc3MgQ2FsY3VsYXRvclVJIGV4dGVuZHMgQ2FsY3VsYXRvciB7XG4gIC8qKlxuICAgKiBUaGUgSFRNTCBlbGVtZW50IHRoYXQgc2hvd3MgdGhlIGNvbnRlbnRzIG9mIHRoZSBjYWxjdWxhdG9yJ3MgZGlzcGxheS5cbiAgICovXG4gIGxjZERpc3BsYXk6IEhUTUxFbGVtZW50O1xuXG4gIGNvbnN0cnVjdG9yKGlkOiBzdHJpbmcpIHtcbiAgICBzdXBlcigpO1xuICAgIHRoaXMubGNkRGlzcGxheSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGlkKTtcbiAgICB0aGlzLmxjZERpc3BsYXkuaW5uZXJIVE1MID0gdGhpcy5sY2Q7XG4gIH1cblxuICBkaWdpdCh4OiBudW1iZXIpOiB2b2lkIHtcbiAgICBzdXBlci5kaWdpdCh4KTtcbiAgICB0aGlzLmxjZERpc3BsYXkuaW5uZXJIVE1MID0gdGhpcy5sY2QudG9TdHJpbmcoKTtcbiAgfVxuXG4gIGRlY2ltYWwoKTogdm9pZCB7XG4gICAgc3VwZXIuZGVjaW1hbCgpO1xuICAgIHRoaXMubGNkRGlzcGxheS5pbm5lckhUTUwgPSB0aGlzLmxjZC50b1N0cmluZygpO1xuICB9XG5cbiAgbmVnYXRlKCk6IHZvaWQge1xuICAgIHN1cGVyLm5lZ2F0ZSgpO1xuICAgIHRoaXMubGNkRGlzcGxheS5pbm5lckhUTUwgPSB0aGlzLmxjZC50b1N0cmluZygpO1xuICB9XG5cbiAgb3AobzogT3ApOiB2b2lkIHtcbiAgICBzdXBlci5vcChvKTtcbiAgICB0aGlzLmxjZERpc3BsYXkuaW5uZXJIVE1MID0gdGhpcy5sY2QudG9TdHJpbmcoKTtcbiAgfVxuXG4gIGVxdWFscygpOiB2b2lkIHtcbiAgICBzdXBlci5lcXVhbHMoKTtcbiAgICB0aGlzLmxjZERpc3BsYXkuaW5uZXJIVE1MID0gdGhpcy5sY2QudG9TdHJpbmcoKTtcbiAgfVxuXG4gIGNsZWFyKCk6IHZvaWQge1xuICAgIHN1cGVyLmNsZWFyKCk7XG4gICAgdGhpcy5sY2REaXNwbGF5LmlubmVySFRNTCA9IHRoaXMubGNkLnRvU3RyaW5nKCk7XG4gIH1cblxuICBjdWJlKCk6IHZvaWQge1xuICAgIHN1cGVyLmN1YmUoKTtcbiAgICB0aGlzLmxjZERpc3BsYXkuaW5uZXJIVE1MID0gdGhpcy5sY2QudG9TdHJpbmcoKTtcbiAgfVxuXG4gIHBlcmNlbnQoKTogdm9pZCB7XG4gICAgc3VwZXIucGVyY2VudCgpO1xuICAgIHRoaXMubGNkRGlzcGxheS5pbm5lckhUTUwgPSB0aGlzLmxjZC50b1N0cmluZygpO1xuICB9XG59IiwiLy8gVGhlIG1vZHVsZSBjYWNoZVxudmFyIF9fd2VicGFja19tb2R1bGVfY2FjaGVfXyA9IHt9O1xuXG4vLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcblx0dmFyIGNhY2hlZE1vZHVsZSA9IF9fd2VicGFja19tb2R1bGVfY2FjaGVfX1ttb2R1bGVJZF07XG5cdGlmIChjYWNoZWRNb2R1bGUgIT09IHVuZGVmaW5lZCkge1xuXHRcdHJldHVybiBjYWNoZWRNb2R1bGUuZXhwb3J0cztcblx0fVxuXHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuXHR2YXIgbW9kdWxlID0gX193ZWJwYWNrX21vZHVsZV9jYWNoZV9fW21vZHVsZUlkXSA9IHtcblx0XHQvLyBubyBtb2R1bGUuaWQgbmVlZGVkXG5cdFx0Ly8gbm8gbW9kdWxlLmxvYWRlZCBuZWVkZWRcblx0XHRleHBvcnRzOiB7fVxuXHR9O1xuXG5cdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuXHRfX3dlYnBhY2tfbW9kdWxlc19fW21vZHVsZUlkXShtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuXHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuXHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG59XG5cbiIsIi8vIGRlZmluZSBnZXR0ZXIgZnVuY3Rpb25zIGZvciBoYXJtb255IGV4cG9ydHNcbl9fd2VicGFja19yZXF1aXJlX18uZCA9IChleHBvcnRzLCBkZWZpbml0aW9uKSA9PiB7XG5cdGZvcih2YXIga2V5IGluIGRlZmluaXRpb24pIHtcblx0XHRpZihfX3dlYnBhY2tfcmVxdWlyZV9fLm8oZGVmaW5pdGlvbiwga2V5KSAmJiAhX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIGtleSkpIHtcblx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBrZXksIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBkZWZpbml0aW9uW2tleV0gfSk7XG5cdFx0fVxuXHR9XG59OyIsIl9fd2VicGFja19yZXF1aXJlX18ubyA9IChvYmosIHByb3ApID0+IChPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqLCBwcm9wKSkiLCIvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG5fX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSAoZXhwb3J0cykgPT4ge1xuXHRpZih0eXBlb2YgU3ltYm9sICE9PSAndW5kZWZpbmVkJyAmJiBTeW1ib2wudG9TdHJpbmdUYWcpIHtcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgU3ltYm9sLnRvU3RyaW5nVGFnLCB7IHZhbHVlOiAnTW9kdWxlJyB9KTtcblx0fVxuXHRPYmplY3QuZGVmaW5lUHJvcGVydHkoZXhwb3J0cywgJ19fZXNNb2R1bGUnLCB7IHZhbHVlOiB0cnVlIH0pO1xufTsiLCJpbXBvcnQgeyBDYWxjdWxhdG9yVUkgfSBmcm9tICcuL0NhbGN1bGF0b3JVSSc7XG5pbXBvcnQgeyBPcCB9IGZyb20gJy4vQ2FsY3VsYXRvcic7XG5cbndpbmRvdy5vbmxvYWQgPSAoKSA9PiB7XG4gIGNvbnN0IGNhbGNVSSA9IG5ldyBDYWxjdWxhdG9yVUkoJ2xjZCcpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnMScpLm9uY2xpY2sgPSAoKSA9PiBjYWxjVUkuZGlnaXQoMSk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCcyJykub25jbGljayA9ICgpID0+IGNhbGNVSS5kaWdpdCgyKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJzMnKS5vbmNsaWNrID0gKCkgPT4gY2FsY1VJLmRpZ2l0KDMpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnNCcpLm9uY2xpY2sgPSAoKSA9PiBjYWxjVUkuZGlnaXQoNCk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCc1Jykub25jbGljayA9ICgpID0+IGNhbGNVSS5kaWdpdCg1KTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJzYnKS5vbmNsaWNrID0gKCkgPT4gY2FsY1VJLmRpZ2l0KDYpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnNycpLm9uY2xpY2sgPSAoKSA9PiBjYWxjVUkuZGlnaXQoNyk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCc4Jykub25jbGljayA9ICgpID0+IGNhbGNVSS5kaWdpdCg4KTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJzknKS5vbmNsaWNrID0gKCkgPT4gY2FsY1VJLmRpZ2l0KDkpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnMCcpLm9uY2xpY2sgPSAoKSA9PiBjYWxjVUkuZGlnaXQoMCk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCcrLScpLm9uY2xpY2sgPSAoKSA9PiBjYWxjVUkubmVnYXRlKCk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCcuJykub25jbGljayA9ICgpID0+IGNhbGNVSS5kZWNpbWFsKCk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCcrJykub25jbGljayA9ICgpID0+IGNhbGNVSS5vcChPcC5BZGQpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnLScpLm9uY2xpY2sgPSAoKSA9PiBjYWxjVUkub3AoT3AuU3ViKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJyonKS5vbmNsaWNrID0gKCkgPT4gY2FsY1VJLm9wKE9wLk11bCk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCcvJykub25jbGljayA9ICgpID0+IGNhbGNVSS5vcChPcC5EaXYpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnPScpLm9uY2xpY2sgPSAoKSA9PiBjYWxjVUkuZXF1YWxzKCk7XG4gIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdDJykub25jbGljayA9ICgpID0+IGNhbGNVSS5jbGVhcigpO1xuICBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnXjMnKS5vbmNsaWNrID0gKCkgPT4gY2FsY1VJLmN1YmUoKTtcbiAgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJyUnKS5vbmNsaWNrID0gKCkgPT4gY2FsY1VJLnBlcmNlbnQoKTtcbn07Il0sIm5hbWVzIjpbXSwic291cmNlUm9vdCI6IiJ9